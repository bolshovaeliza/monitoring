# Monitoring

This project is an illustration of some tools available for monitoring system status. It is optimized to run on on a virtual machine *Ubuntu Server 20.04 LTS*.

This project is broken up into 10 different blocks. Each folder contains `main.sh` scripts, as well as other supplementary scripts and files.

Folders `01` and `02` contain bash scripts that allow one to test results of loading the system with excessive file creation. Scripts in folder `01` create a certain number of files and folders, while scripts in folder `02` clog the system until 1GB of free space is left. Use scripts in folder `03` to clean up any unwanted files.

Scripts in `04` generate 5 files of random *nginx* logs in combined format. You can use these to test programs in folders `05` and `06` that custom provide parsing tools and generation of GoAccess analytics respectively.

Finally, folder `10` contains commentary on setting up a monitoring system using *Grafana* and *Prometheus*. In folder `07` tests, custom *Grafana* dashboard and results of said tests as screenshots can be found. In folder `08` result of the same tests are shown using *Node Exporter Quickstart and Dashboard* that can be downloaded from *Grafana Labs*. Folder `09` contains necessary files for a monitoring system that uses a bash script to gather custom metrics.
