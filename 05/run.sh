#!/bin/bash

args="test"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./main.sh $args
echo

args="1 r"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./main.sh $args
echo

args="5"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./main.sh $args
echo

# args="1"
# echo
# echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
# echo "VALID ARGUMENTS ($args):"
# bash ./clean.sh
# bash ./main.sh $args
# cat *.log
# echo

# args="2"
# echo
# echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
# echo "VALID ARGUMENTS ($args):"
# bash ./clean.sh
# bash ./main.sh $args
# cat *.log
# echo

# args="3"
# echo
# echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
# echo "VALID ARGUMENTS ($args):"
# bash ./clean.sh
# bash ./main.sh $args
# cat *.log
# echo

# args="4"
# echo
# echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
# echo "VALID ARGUMENTS ($args):"
# bash ./clean.sh
# bash ./main.sh $args
# cat *.log
# echo