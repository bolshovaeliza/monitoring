# !/bin/bash

. ./validation.sh

validation $@

case $1 in
  1) awk '{print $0 | "sort -k 9"}' $log_file > sorted.log ;;
  2) awk '!a[$1]++ {print $1}' $log_file > unique_ips.log ;;
  3) awk '$9 >= 400 && $9 < 600 {print $0}' $log_file > erroneous_requests.log ;; 
  4) awk '$9 >= 400 && $9 < 600 && !a[$1]++ {print $1}' $log_file > unique_ips_in_erroneous_requests.log ;; 
esac