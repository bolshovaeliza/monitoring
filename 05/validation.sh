#!/bin/bash

log_file="$(realpath "$(dirname "$0")")/../04/access_?.log"

function usage {
  echo "Usage: $0 <S1> "
  echo "Required parameters:"
  echo "    S1 - Type of parsing protocol"
  echo "         1 - All entries sorted by response code"
  echo "         2 - All unique IPs found in the entries"
  echo "         3 - All requests with errors (response code - 4xx or 5xxx)" 
  echo "         4 - All unique IPs found among the erroneous requests"
  echo
}

function validation {
  result=0
  if [ "$#" -lt 1 ]; then
    echo "ERROR: Not enough arguments."
    usage
    exit 1
  fi
  if [ "$#" -gt 1 ]; then
    echo "ERROR: Too many arguments."
    result=1
  fi
  if [[ ! $1 =~ ^[1-4]$ ]]; then
    echo "ERROR: first argument must be 1, 2, 3 or 4"
    result=1
  fi
  if ! ls $log_file >/dev/null 2>&1; then
    echo "ERROR: Unable to find $log_file"
    
  fi
    
  if [ $result -ne 0 ]; then
    usage
    exit $result
  fi
  

}
