#!/bin/bash

function usage {
  echo "Usage: $0 <S1> <S2> <S3>"
  echo
  echo "Required parameters:"
  echo "   S1  a list of English alphabet letters used in folder names (no more than 7 characters)."
  echo "   S2  a list of English alphabet letters used in the file name and extension (no more than"
  echo "       7 characters for the name, no more than 3 characters for the extension)."
  echo "   S3  file size (in Megabytes, but not more than 100)."
  echo
}

function validation {
  result=0
  error+=""
  if [ "$#" -lt 3 ]; then
    echo "ERROR: Not enough arguments."
    echo
    usage
    exit 1
  fi

  if [[ "$1" =~ [^a-zA-Z] ]]; then
    error+="ERROR: First argument must contain only English letters.\n"
    result=1
  fi

  if [ ${#1} -gt 7 ]; then
    error+="ERROR: First argument must be no more than 7 characters long.\n"
    result=1
  fi 

  if [[ "$2" =~ ^[a-z]{1,7}$ ]] || [[ ! "$2" =~ ^[a-z]{1,7}.[a-z]{1,3}$ ]]; then
    error+="ERROR: Second argument must contain only English letters and a dot.\n"
    result=1
  fi

  if ! [[ $3 =~ ^[0-9.]+Mb$ ]]; then
    error+="ERROR: Third argument must be in Mb format.\n"
    result=3
  fi

  filesize=$6
  only_size=${filesize%Mb}
  if [[ $only_size -ge 100 ]]; then 
    error+="ERROR: Third argument must be less than 100Mb.\n"
    result=3
  fi

  if [ $result -ne 0 ]; then
    echo -e "$error"
    usage
    exit $result
  fi

}
