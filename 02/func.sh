#!/bin/bash
LOG_FILE=$(realpath "$(dirname "$0")")/log_file.log

folder_name_letters=$1
file_name_letters=$2
file_size=$3
path=~/

function get_free_space_mb {
  free_space=$(df -h -BMB / | awk 'FNR == 2 {print $4}' | sed 's/[a-zA-Z]//g')
  echo $free_space
}

function create_folders {
  # echo "${#path}"
  directories=$(find ${path} -maxdepth 3 -mindepth 1 -type d)
  directories+="  $path"
  free_space=$(get_free_space_mb)
  for dir in $directories; do
    for (( i=0; i<$(( $RANDOM % 3 + 1 )); i++ )); do
      if [[ $dir != *bin* ]] && (( $free_space > 1000 )); then
        folder_name=$(generate_foldername)
        new_path="$dir/$folder_name"
        if [ ! -d "$new_path" ]; then
            mkdir -p $new_path
        fi
        echo "Folder - $(realpath $new_path) - $(date "+%Y-%m-%d %H:%M") - $(du -h $new_path | awk '{print $1}')" >> $LOG_FILE
      fi
      free_space=$(get_free_space_mb)
    done
  done
}

function create_files {
  free_space=$(get_free_space_mb)
  for dir in $(find "${path}" -maxdepth 100 -mindepth 1 -type d -name "*$(date +"%d%m%y")*"); do
    created_files=0
    while (( $free_space > 1000 )) && [[  $created_files -le $(( $RANDOM % 10 + 1 )) ]]; do
      file_name=$(generate_filename)
      if [ ! ${#file_name} -gt 100 ]; then
        fallocate -l $file_size "$dir/$file_name"
        echo "File - $(realpath "$dir/$file_name") - $(date "+%Y-%m-%d %H:%M") - $(du -h "$dir/$file_name" | awk '{print $1}')" >> $LOG_FILE
      fi
      ((created_files++))
      free_space=$(get_free_space_mb)
    done
  done

}

function generate_foldername {
  folder_name=""
  count=${#folder_name_letters}
  for (( j=0; j<${#folder_name_letters}; j++ )); do
    current_char="${folder_name_letters: j:1}"
    for (( k=0; k<$((2 + $RANDOM % (9 - $count))); k++ )); do
      folder_name+=$current_char;
    done
  done

  if [[ ${#folder_name} -lt 4 ]]; then
    for (( k=0; k<$((4 + $RANDOM % 11)); k++ )); do
      folder_name+="${folder_name_letters:0:1}"
    done
  fi

  folder_name+="_$(date +"%d%m%y")"
  echo "$folder_name"
}

function generate_filename {
  file_name=""
  name_letters="${file_name_letters%%.*}" 
  name_letters_count=${#name_letters}
  for (( j=0; j<name_letters_count; j++ )); do
    current_char="${name_letters: j:1}"
    for (( k=0; k<$((2 + $RANDOM % 23)); k++ )); do
      file_name+=$current_char;
    done
  done

  if [[ ${#file_name} -lt 4 ]]; then
    for (( k=0; k<$((4 + $RANDOM % 21)); k++ )); do
      file_name+="${name_letters:0:1}"
    done
  fi
  file_name+="_$(date +"%d%m%y")"

  extention="${file_name_letters#*.}"
  file_name+=".${extention}"

  echo "$file_name"
}

function main_func { 
  start_time=`date +%T`
  start_count_time=`date +%s`
  space_before=$(get_free_space_mb)


  free_space=$(get_free_space_mb)
  if [[ $free_space < "1.0" ]]; then
    echo "Not enough free space on the drive."
  else
    while (( $free_space > 1000 )); do  
      create_folders 
      create_files
    done
  fi

  end_time=`date +%T`
  end_count_time=`date +%s`
  runtime=$((end_count_time-start_count_time))

  echo "   RUN RESULTS:" >> $LOG_FILE
  echo "START: $start_time" >> $LOG_FILE
  echo "FINISH: $end_time" >> $LOG_FILE
  echo "DURATION: $runtime" >> $LOG_FILE

  space_after=$(get_free_space_mb)
  echo "SPACE BEFORE: $space_before Mb" >> $LOG_FILE
  echo "SPACE AFTER: $space_after Mb" >> $LOG_FILE
  exit 0
}