#!/bin/bash
args="test abc 0 ax.ttx 16"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="abc def.ttx"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="привет_дорогой мы_знакомы_или_нет.ttx 10"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="test 2 abc 0 ax.ttx 16"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo


args="abc def.ttx 100Mb"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "VALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
cat *.log
echo
