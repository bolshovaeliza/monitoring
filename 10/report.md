# AMMOSHRI REPORT: HOW TO SET THIS UP

Herein lie the instuctions to set up the system necessary to replicate this project.

## VM settings
This example uses *VirtualBox*.

You need two virtual machines running *Ubuntu Server 20.04 LTS*.

### Shared folders

At least one of these machines should have a shared folder with the host machine. It is set up in settings of the machine in *VirtualBox* and inside the machine itself.

These are the necessary chenges inside VirtualBox:
![shared_folders_virtualbox](images/DO5-extra-01-shared_folders_virtualbox.png)

You need to remember the name you give to the shared folder inside the *VirtualBox* interface. It will be used later when mounting the folder into your machine using this command:

```sudo mount -t vboxsf <shared_folder_name> <vm_folder_name>```

Replace `<vm_folder_name>` with any folder on your vm.

---

### Network configuration

Both of these machines need to have their network configuration changed. 

You need to first enable multiple network interfaces in *VirtualBox* settings. It is done in the *Settings* menu of each machine, in the folder network. First adapter is default, used for connection to the Internet. Second adapter will be used to connect the machines to each other. It should be attached to internal network (make sure that the network name for both machines is the same!).

![adapter_1](images/DO5-extra-02-adapter_1.png)

![adapter_2](images/DO5-extra-03-adapter_2.png)

Inside your machines call `ip a` command. It will allow you to determine names of your network interfaces (aka adapters you set up before).

You need to change `/etc/netplan/00-installer-config.yaml` files of both machines to the ones in this folder (change only the contents, not the names).

After changing config files call `sudo netplan apply` on both machines. This will apply new settings.

Also call `ip route` and make sure that you have only one default route, and it doesn't go to your other machine. This will ensure that during installation you will have no problems fetching libraries you need. You will still be able to ping your machine if you specify the interface like so:

```ping -I <interface> <address>```

For example, i could call this command like `ping -I enp0s8 172.24.116.8`.

To delete an unnecessary interface you can call this command: 

```sudo ip route del default via <address> dev <interface>```

In my case, *address* was 192.168.1.1 and *interface* was enp0s8.

---

### Grafana and Prometheus

To install *Grafana* and *Prometheus* you can use the scripts in folder *src/07*. These two bugges run together, *Prometheus* to gather data and *Grafana* to display, so you need both. 

In *VirtualBox* you need to setup port forwarding. Grafana is set up on port 9090 by default, and Prometheus has port 3000 set as default.

![port_forwarding](images/DO5-extra-04-port_forwarding.png)

After the installation is complete, you can run both *Prometheus* and *Grafana* using 

```bash
sudo systemctl start grafana-server
sudo systemctl start prometheus
```

You can use `sudo systemctl status <program>` to check that *Prometheus* and *Grafana* are running.

You don't really need *Prometheus* web interface for this project, but you do need *Grafana*. If you set everything correctly, you can open it by accessing `http://localhost:3000/` in your web brouser. You will be required to log in. Default login is *admin* with password *admin* (convenient, right?).

On this stage you need to connect data from *Prometheus*. To do that, you look up the cog in the left menu, and choose option "Data sources". There, you need to add *Prometheus* as a data source. The bare minimum you have to do i specify `http://localhost:9090/` in *URL* input line and click "Save & Test" on the bottom of the screen. Hopefully, you have the message "Data source is working".

![data_sources](images/DO5-extra-05-data_sources.png)

You then need to go to "Dashboards", create a new one and add all the necessary data to it. Dashboards consist of panels (graphs/widgets that display the info). Each panel has a data source, in this case it will be our dear old *Prometheus*.

![panel_example](images/DO5-extra-06-panel_example.png)

You need the following metrics for part 7:
- node_cpu_seconds_total
- node_filesystem_avail_bytes
- node_memory_MemAvailable_bytes
- node_disk_reads_completed_total
- node_disk_writes_completed_total

> There seems to be a way to display multiple metrics on the same panel, but i coult not be bothered.

### Iperf3

To run iperf 3 you need to call `ipefr3 -s -f m` on your server machine (the same one that's running *Grapana* and *Prometheus*), and `iperf3 -c <vm_ip>` on another machine connected to it.

### Node exporter

The only thing I had to do (i think) is just install *Prometheus*, and *Node exporter* will come with. You need only to import the dashboard in *Grafana*'s web interface.

### Custom node exporter


You need several components to run this. First, you need to add a scrape job to `/etc/prometheus/prometheus.yml`.

```bash 
scrape_configs:

    [your other jobs]

    - job_name: custom_node
    metrics_path: /
    static_configs:
      - targets: ['localhost:81']
```

You also need to add a server to `/etc/nginx/nginx.conf` like so. I changed nothing else in the file.

```conf
http { # this was already in the file
        # other stuff
        server {
                listen 81;
                location / {
                          root /var/www/html/metrics;
                          index index.html;
                }
        }

        #other stuff
}
```

When changing both of these files you need to be mindful of the fact that port number (aka `81`) and metrics location (aka `/`) is the same in both files. `root` should contain the path to your script output file. The file should be in folder /var/www/html somewhere, so that nginx can see it (i think?).

To apply changes restart *Prometheus* and *nginx* using `sudo systemctl restart <program>`.

Now onto generating the .html file. I strongly recommend prefixing names of your metrics with, say, "s21_" to make searching it in *Grafana* easier.
 
You can also run scripts in the background if you call them with an `&` like so:

```bash <script_name>.sh &```

It will output pid (process id) that you can later use to terminate the process:

```kill <pid>```

Should you forget the pid, call this command

```pgrep -f <script_name>.sh```

