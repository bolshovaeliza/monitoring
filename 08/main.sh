#!/bin/bash

if [ "$#" -ne 0 ]; then
  echo "ERROR: too many arguments."
  exit 1
fi

cd $(realpath "$(dirname "$0")")/../07
main.sh
echo "Going to start listening using iperf3, please set up client machine"
iperf3 -s -f m
