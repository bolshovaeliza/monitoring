#!/bin/bash

if [ "$#" -ne 0 ]; then
  echo "ERROR: too many arguments."
  exit 1
fi

cd $(realpath "$(dirname "$0")")/../02
bash main.sh abc def.ttx 100Mb
echo "Starting stress test soon, please stand by."
sleep 15
stress -c 2 -i 1 -m 1 --vm-bytes 32M -t 10s
