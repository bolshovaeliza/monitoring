#!/bin/bash

# Dependencies:
sudo apt install -y stress
sudo apt install -y libfontconfig1
sudo apt-get install -y adduser

# Grafana:
sudo wget https://dl.grafana.com/oss/release/grafana_9.4.7_amd64.deb 
sudo dpkg -i grafana_9.4.7_amd64.deb
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable grafana-server
sudo /bin/systemctl start grafana-server
