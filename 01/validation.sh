#!/bin/bash

function usage {
  echo "Usage: $0 <S1> <S2> <S3> <S4> <S5> <S6>"
  echo
  echo "Required parameters:"
  echo "   S1  the absolute path."
  echo "   S2  the number of subfolders."
  echo "   S3  a list of English alphabet letters used in folder names (no more than 7 characters)."
  echo "   S4  the number of files in each created folder."
  echo "   S5  the list of English alphabet letters used in the file name and extension (no more than"
  echo "       7 characters for the name, no more than 3 characters for the extension)."
  echo "   S6  file size (in kilobytes, but not more than 100)."
  echo
}

function validation {
  result=0
  error+=""
  if [ "$#" -lt 6 ]; then
    echo "ERROR: Not enough arguments."
    echo
    usage
    exit 1
  fi

  if [ "$#" -gt 6 ]; then
    error+="ERROR: Too many arguments.\n"
    result=1
  fi

  if [ ! -d "$1" ]; then
    error+="ERROR: First argument must be a directory.\n"
    result=1
  fi

  if [[ ! "$2" =~ ^[0-9]+$ ]]; then
    error+="ERROR: Second argument must be an integer.\n"
    result=1
  fi

  if [[ "$3" =~ [^a-zA-Z] ]]; then
    error+="ERROR: Third argument must contain only English letters.\n"
    result=1
  fi

  if [ ${#3} -gt 7 ]; then
    error+="ERROR: Third argument must be no more than 7 characters long.\n"
    result=1
  fi

  if [[ ! "$4" =~ ^[0-9]+$ ]]; then
    error+="ERROR: Fourth argument must be an integer.\n"
    result=1
  fi

  if [[ "$4" =~ ^[0-9]+$ ]] && [[ ! "$4" -ne 0 ]]; then
    error+="ERROR: Fourth argument must not be 0.\n"
    result=1
  fi

  if [[ "$5" =~ ^[a-z]{1,7}$ ]] || [[ ! "$5" =~ ^[a-z]{1,7}.[a-z]{1,3}$ ]]; then
    error+="ERROR: Fifth argument must contain only English letters and a dot.\n"
    result=1
  fi

  if ! [[ $6 =~ ^[0-9.]+kb$ ]]; then
    error+="ERROR: Sixth argument must be in kb format.\n"
    result=3
  fi

  filesize=$6
  only_size=${filesize%kb}
  if [[ $only_size -ge 100 ]]; then 
    error+="ERROR: Sixth argument must be less than 100kb.\n"
    result=3
  fi

  if [ $result -ne 0 ]; then
    echo -e "$error"
    usage
    exit $result
  fi

}
