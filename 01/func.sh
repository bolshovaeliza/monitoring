#!/bin/bash
path=$1
folder_number=$2
folder_name_letters=$3
file_number=$4
file_name_letters=$5

LOG_FILE=$(realpath "$(dirname "$0")")/log_file.log

function create_folders {
	for (( i=0; i<folder_number; i++ )); do
    folder_name=$(generate_foldername)
    mkdir -p $path/$folder_name
    echo "Folder - $path/$folder_name/ - $(date +"%d-%m-%Y") - $(du -h $path/$folder_name/ | awk '{print $1}')" >> $LOG_FILE
    folder_name=""
	done
}

function generate_foldername {
  folder_name=""
  count=${#folder_name_letters}
  for (( j=0; j<${#folder_name_letters}; j++ )); do
    current_char="${folder_name_letters: j:1}"
    for (( k=0; k<$((2 + $RANDOM % (9 - $count))); k++ )); do
      folder_name+=$current_char;
    done
  done

  if [[ ${#folder_name} -lt 4 ]]; then
    for (( k=0; k<$((4 + $RANDOM % 11)); k++ )); do
      folder_name+="${folder_name_letters:0:1}"
    done
  fi

  folder_name+="_$(date +"%d%m%y")"
  echo "$folder_name"
}

function create_files {
	file_name=""
	for dir in $(find $path -maxdepth 1 -mindepth 1 -type d | grep $(date +"%d%m%y")); do
		file_counter=0
		while [ $file_counter -lt $file_number ]; do
			if [[ $free_space > "1.0" ]]; then
				file_name=$(generate_filename)
				fallocate -l $only_size"KB" "$dir/$file_name"
				((file_counter++))
				echo "File - $dir/$file_name - $(date "+%Y-%m-%d %H:%M") - $(du -b $dir/$file_name| awk '{print $1}' | sed 's/.$//' | sed 's/.$//' | sed 's/.$//') kb" >> $LOG_FILE
			else
				echo "Less than 1GB free space on the drive."
				exit 1
			fi
      free_space=$(df -h / | awk 'FNR == 2 {print $4}' | sed 's/.$//')
		done
	done
}

function generate_filename {
  file_name=""
  name_letters="${file_name_letters%%.*}" 
  name_letters_count=${#name_letters}
  for (( j=0; j<name_letters_count; j++ )); do
    current_char="${name_letters: j:1}"
    for (( k=0; k<$((2 + $RANDOM % (13 - $name_letters_count))); k++ )); do
      file_name+=$current_char;
    done
  done

  if [[ ${#file_name} -lt 4 ]]; then
    for (( k=0; k<$((4 + $RANDOM % 14)); k++ )); do
      file_name+="${name_letters:0:1}"
    done
  fi
  file_name+="_$(date +"%d%m%y")"

  extention="${file_name_letters#*.}"
  file_name+=".${extention}"

  echo "$file_name"
}

function main_func {
    free_space=$(df -h / | awk 'FNR == 2 {print $4}' | sed 's/.$//')

    if [[ $free_space < "1.0" ]]; then
        echo "Not enough free space on the drive."
    else
        create_folders $path $folder_number $folder_name_letters
        create_files $file_number $file_name_letters $only_size
    fi
}