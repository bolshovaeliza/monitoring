#!/bin/bash

args="not_a_folder a ыыыыыыыыыы f a3403304x.ttx 1000kb"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="test 2"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="test 2 abc 2 ax.ttx 16kb 3"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo

args="test 2 abc 0 ax.ttx 16"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
echo


args="test 2 abc 2 ax.ttx 16kb"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "VALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
cat *.log
echo

args="test 4 abc 5 ax.ttx 50kb"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "VALID ARGUMENTS ($args):"
bash ./clean.sh
mkdir -p test
bash ./main.sh $args
cat *.log
echo
