#!/bin/bash


function generate_ip {
  echo $((RANDOM%256)).$((RANDOM%256)).$((RANDOM%256)).$((RANDOM%256))
}

function generate_url {
  domains=("goaccess.io" "edu.21-school.ru" "21-school-by-students.notion.site" "repos.21-school.ru" "stackoverflow.com")
  paths=("/" "/dashboard" "/about" "/contact" "/projects" "/students")
  params=("" "id=1" "name=bagshotw" "page=2" "category=books" "search=term")

  domain=${domains[$RANDOM % ${#domains[@]} ]}
  path=${paths[$RANDOM % ${#paths[@]} ]}

  param=""
  for i in {1..3}; do
    param+="&${params[$RAND~OM % ${#params[@]}]}"
  done

  echo "https://$domain$path?$param"
}

function output_code_descriptions {
  echo -e "Коды ответа:
    200 - OK (успешный запрос)
    201 - Created (успешное создание ресурса)
    400 - Bad Request (некорректный запрос)
    401 - Unauthorized (неавторизованный запрос)
    403 - Forbidden (запрещенный запрос)
    404 - Not Found (ресурс не найден)
    500 - Internal Server Error (внутренняя ошибка сервера)
    501 - Not Implemented (не реализовано на сервере)
    502 - Bad Gateway (некорректный ответ от прокси-сервера)
    503 - Service Unavailable (сервис недоступен)"

}

response_codes=(200 201 400 401 403 404 500 501 502 503)
methods=("GET" "POST" "PUT" "PATCH" "DELETE")
user_agents=("Mozilla" "Google Chrome" "Opera" "Safari" "Internet Explorer" "Microsoft Edge" "Crawler and bot" "Library and net tool")

for i in {1..5}
do
  num_records=$((RANDOM%901+100))

  date=$(date -d "-$((5-i)) day" +"%d/%b/%Y")

  for ((j=1;j<=$num_records;j++))
  do
    time=$(date -d "-$((num_records/5-j)) minute" +"%H:%M:%S +0000")

    ip=$(generate_ip)
    response_code=${response_codes[$((RANDOM%10))]}
    method=${methods[$((RANDOM%5))]}
    url=$(generate_url)
    user_agent=${user_agents[$((RANDOM%8))]}

    echo "$ip - - [$date:$time] \"$method $url HTTP/1.1\" $response_code - \"$user_agent\"" >> "$(realpath "$(dirname "$0")")/access_$i.log"

  done
done


