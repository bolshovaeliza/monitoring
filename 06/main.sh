#!/bin/bash

if [ "$#" -ne 0 ]; then
  echo "ERROR: too many arguments."
  exit 1
fi

log_file="$(realpath "$(dirname "$0")")/../04/access_?.log" 
goaccess $log_file -a --log-format=COMBINED -o report.html
