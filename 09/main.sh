#!/bin/bash

metrics_file="/var/www/html/metrics/index.html"

function gen_html() {
  if ! [[ -d /var/www/html/metrics ]]; then
    mkdir /var/www/html/metrics -p
  fi

  cpu_usage=$(cat <(grep 'cpu ' /proc/stat) <(sleep 1 && grep 'cpu ' /proc/stat) | awk -v RS="" '{print ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}')

  mem_total=$(free -m | awk 'NR==2{print $2}')
  mem_used=$(free -m | awk 'NR==2{print $3}')
  mem_free=$(free -m | awk 'NR==2{print $4}')

  disk_total=$(df  | awk '$NF=="/"{printf "%.3f", $2/1000000}')
  disk_used=$(df  | awk '$NF=="/"{printf "%.3f", $3/1000000}')
  disk_free=$(df  | awk '$NF=="/"{printf "%.3f", $4/1000000}')

  cat <<EOF > $metrics_file
# HELP cpu_usage CPU usage in percent
# TYPE cpu_usage gauge
s21_cpu_usage $cpu_usage
# HELP mem_total Total memory in MB
# TYPE mem_total gauge
s21_mem_total $mem_total
# HELP mem_used Used memory in MB
# TYPE mem_used gauge
s21_mem_used $mem_used
# HELP mem_free Free memory in MB
# TYPE mem_free gauge
s21_mem_free $mem_free
# HELP disk_total Total disk space in GB
# TYPE disk_total gauge
s21_disk_total $disk_total
# HELP disk_used Used disk space in GB
# TYPE disk_used gauge
s21_disk_used $disk_used
# HELP disk_free Free disk space in GB
# TYPE disk_free gauge
s21_disk_free $disk_free
EOF
}

function main() {
  if [ "$#" -ne 0 ]; then
    echo "ERROR: too many arguments."
    exit 1
  fi
  rm -rf $metrics_file
  while true; do
    gen_html
    sleep 3
  done
}

main
