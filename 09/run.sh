#!/bin/bash

if [ "$#" -ne 0 ]; then
  echo "ERROR: too many arguments."
  exit 1
fi

cd $(realpath "$(dirname "$0")")/../07
main.sh