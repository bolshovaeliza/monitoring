#!/bin/bash

function usage {
  echo
  echo "Usage: $0 <S1> <S2>"
  echo
  echo "Required parameters:"
  echo "   S1  Type of cleaning protocol"
  echo "       1 - By log file"
  echo "       2 - By creation date and time"
  echo "       3 - By name mask (i.e. characters, underlining and date)"
  echo 
}

function validation {

  result=0
  if [ "$#" -lt 1 ]; then
    echo "ERROR: Not enough arguments."
    usage
    exit 1
  fi
  if [ "$#" -gt 1 ]; then
    echo "ERROR: Too many arguments."
    result=1
  fi

  if [[ ! "$1" =~ ^[1-3]  ]]; then
      echo "ERROR: Argument must be 1, 2 or 3."
      result=1
  else
      if [[ $1 -ne 1 && $1 -ne 2 && $1 -ne 3 ]]; then
          echo "ERROR: Argument must be 1, 2 or 3."
          result=1
      fi
  fi
  
  if [ $result -ne 0 ]; then
    usage
    exit $result
  fi

}