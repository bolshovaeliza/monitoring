#!/bin/bash

date_name=`date +"%d%m%y"`
date_log=`date +"%d.%m.%Y"`

function del_by_log() {
  read -p "Input path to .log file: " log_file
  echo "Removing files using the following .log file:" $log_file
  if [[ -f $log_file ]]; then
    cat $log_file | awk '{print($1)}' | grep / | xargs rm -rf
    echo "" > $log_file
  else echo "ERROR: File does not exist"
  fi
  echo "I take it you can remove the .log file from the folder yourself."
}

function input_time() {
  while true; do
    read -p "Input $1 time in format HH:MM (take into account the time zone of your device)." t
    
    if [[ $t =~ ^([01]?[0-9]|2[0-3]):[0-5][0-9]$ ]]; then
      break
    fi

    echo -e "\tTime should be in 24h format"
  done 
}

function del_by_time() {
  read -p "Input date of file creation: " date_log
  date_log=`date +"%Y-%m-%d"`

  t=""

  input_time "begin"
  start_time=$t

  input_time "end"
  end_time=$t

  echo "Start date and time:" $date_log $start_time
  echo "End   date and time:" $date_log $end_time

  find / -type d -newermt "$date_log $start_time:00" ! -newermt "$date_log $end_time:00" 2>/dev/null | grep -v media | xargs rm -rf 2>/dev/null
}

function del_by_mask() {
  read -p "Input mask: " mask
  echo "Removing files using the following mask:" $mask
  find / -type f -name "*$mask*" -delete 2>/dev/null
  find / -type d -name "*$mask*" -exec rm -rf {} \; 2>/dev/null
}

function main_func {

  case $1 in
    1) del_by_log ;;
    2) del_by_time ;;
    3) del_by_mask ;;
  esac

}