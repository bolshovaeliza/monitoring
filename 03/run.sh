#!/bin/bash

args="test"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash $(realpath "$(dirname "$0")")/main.sh $args
echo

args="1 r"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash $(realpath "$(dirname "$0")")/main.sh $args
echo

args="4"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "INVALID ARGUMENTS ($args):"
bash $(realpath "$(dirname "$0")")/main.sh $args
echo

args="1"
echo
echo "--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--/\/--"
echo "VALID ARGUMENTS ($args):"
echo " SUGGESTION: Try to clean using ../02/log_file.log"
bash $(realpath "$(dirname "$0")")/main.sh $args
echo
