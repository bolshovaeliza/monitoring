#!/bin/bash
. $(realpath "$(dirname "$0")")/validation.sh
. $(realpath "$(dirname "$0")")/func.sh

validation $@
main_func $@

echo "SUCCESS"
exit 0